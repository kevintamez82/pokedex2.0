import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import VueResource from 'vue-resource'
import i18n from './i18n'
require('./../node_modules/vuetify/dist/vuetify.min.css')
require('./../node_modules/vuetify/dist/vuetify.js')
require('./icons/font-awesome-4.7.0/css/font-awesome.css')
// import('./../node_modules/vuetify/dist/vuetify.min.css')
// import('./../node_modules/vuetify/dist/vuetify.js')
// import('./icons/font-awesome-4.7.0/css/font-awesome.css')

Vue.use(VueResource)
Vue.use(Vuetify)

Vue.config.productionTip = true
Vue.http.options.root = 'https://pokeapi.co/api/v2/'
// Vue.http.options.root='https://www.restaurantea.com/public'
Vue.http.headers.common['Access-Control-Allow-Origin'] = 'https://pokeapi.co/api/v2/'
new Vue({
  i18n,
  render: h => h(App)
}).$mount('#app')
