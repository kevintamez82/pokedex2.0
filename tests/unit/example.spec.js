import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import pokemonView from '@/components/pokemonView.vue'

describe('PokemonView.vue is showing a keyword', () => {
  it('renders Weight in view', () => {
    const msg = 'Weight'
    const wrapper = shallowMount(pokemonView, {
      propsData: {
        msg,
        pokemon: {
          name: 'charisard',
          sprites: {
            front_default: 'front'
          }
        }
      }
    })
    expect(wrapper.text()).to.include(msg)
  })
})
